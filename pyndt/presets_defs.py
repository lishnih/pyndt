#!/usr/bin/env python
# coding=utf-8

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )


# Rev. 20130903


defs = dict()


# Сварочные журналы
defs["pgu235/СП_Астрахань_МехМонт_Сварочный-журнал_001"] = dict(
    xls_type = {
        'domain':           "Welding",
        'method':           "Welding",
        'form':             "SNiP",
        'table':            None,
        'fields':           [''],
    },

    xls_formsearch = {
        'name':       [ 0,  0],
    },

    # Проверка столбца в таблице, если пусто, то строка пропускается
    name_check = 3,

    # До какой колонки в листе включительно искать запись
    xls_namemax = 0,

    # Номер колонки с номером стыка (-1 - неизвестно)
    xls_name_row = 3,

    xls_linesearch = [
    # 1. start     после этой строки + start_offset будут искаться стыки
    #              число - номер строки, текст - содержимое ячейки
    # 2. stop      и до этой строки
    # 3. start_offset
        2,
        10000,
        0
    ],

    # Данные для определения колонок таблицы
    xls_col_table_def = [
    ],
)


# Журналы неразрушающего контроля
defs["pgu235/СП_Астрахань_ОКК_Журнал-НК_001"] = dict(
    xls_type = {
        'domain':           "NDT",
        'method':           "NDT",
        'form':             "SNiP",
        'table':            None,
        'fields':           [''],
    },

    xls_formsearch = {
        'name':       [ 0,  0],
    },

    # Проверка столбца в таблице, если пусто, то строка пропускается
    name_check = 1,

    # До какой колонки в листе включительно искать запись
    xls_namemax = 0,

    # Номер колонки с номером стыка (-1 - неизвестно)
    xls_name_row = 1,

    xls_linesearch = [
    # 1. start     после этой строки + start_offset будут искаться стыки
    #              число - номер строки, текст - содержимое ячейки
    # 2. stop      и до этой строки
    # 3. start_offset
        3,
        10000,
        0
    ],

    # Данные для определения колонок таблицы
    xls_col_table_def = [
    ],
)


# Стилоскопирование
defs["pgu235/СП_Астрахань_ОКК_Протокол-РК_001"] = dict(
    xls_type = {
        'domain':           "DT",
        'method':           "ST",
        'form':             "RD",
        'table':            None,
        'fields':           [''],
    },

    xls_formsearch = {
        'proj_name':  [ 2,  8],
        'obj_name':   [ 3,  8],
        'employee':   [ 4,  8],

        'name':       [ 8,  8],
        'report1':    [ 8,  7],
        'report2':    [ 8,  9],
        'date_str':   [10,  0],
    },

    # До какой колонки в листе включительно искать запись
    xls_namemax = 2,

    # Номер колонки с номером стыка (-1 - неизвестно)
    xls_name_row = 1,

    xls_linesearch = [
    # 1. start     после этой строки + start_offset будут искаться стыки
    #              число - номер строки, текст - содержимое ячейки
    # 2. stop      и до этой строки
    # 3. start_offset
        19,
        32,
        0
    ],

    # Данные для определения колонок таблицы
    xls_col_table_def = [
    ],
)


defs["pgu235/СП_Астрахань_<Отдел>_<Документ>_001"] = \
    defs["pgu235/СП_Астрахань_ОКК_Протокол-РК_001"]


defs["pgu235/СП_Астрахань_ОКК_Журнал-РК_001"] = dict(
    xls_type = {
        'domain':           "DT",
        'method':           "ST",
        'form':             "RD",
        'table':            None,
        'fields':           [''],
    },

    xls_formsearch = {
        'proj_name':  [ 2,  8],
        'obj_name':   [ 3,  8],
        'employee':   [ 4,  8],

        'name':       [ 8,  8],
        'report1':    [ 8,  7],
        'report2':    [ 8,  9],
        'date_str':   [10,  0],
    },

    # До какой колонки в листе включительно искать запись
    xls_namemax = 2,

    # Номер колонки с номером стыка (-1 - неизвестно)
    xls_name_row = 1,

    xls_linesearch = [
    # 1. start     после этой строки + start_offset будут искаться стыки
    #              число - номер строки, текст - содержимое ячейки
    # 2. stop      и до этой строки
    # 3. start_offset
        19,
        32,
        0
    ],

    # Данные для определения колонок таблицы
    xls_col_table_def = [
    ],
)
