#!/usr/bin/env python
# coding=utf-8

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )


pluginspath = "$/plugins"


EXPORT = dict(
    ndt_xls_path = r"C:\\Users\SP0025\Заключения",
    def_default  = "pgu235/СП_Астрахань_ОКК_Журнал-НК_001",

    def_func     = None,

    sqlpath      = "./sql",
    logspath     = "./logspath",

#   dbtype       = "sqlite",
#   dbname       = "pgu235",
#   path         = "./",

    dbtype       = "mysql",
    dbname       = "pgu235",
    host         = "localhost",
    user         = "root",
    passwd       = "54321"
)


EXPORT_WELDING = dict(
    ndt_xls_path = r"D:\opt\home\pgu235\Сводная таблица\Сварочные журналы ПГУ",
    def_default  = "pgu235/СП_Астрахань_МехМонт_Сварочный-журнал_001",

    def_func     = None,

    sqlpath      = "./sql",
    logspath     = "./logspath",

    dbtype       = "mysql",
    dbname       = "pgu235_welding",
    host         = "localhost",
    user         = "root",
    passwd       = "54321"
)


EXPORT_DT = dict(
    ndt_xls_path = r"D:\opt\home\pgu235\ЛабораторияРК\Протоколы",
    def_default  = "pgu235/СП_Астрахань_ОКК_Журнал-РК_001",

    def_func     = None,

    sqlpath      = "./sql",
    logspath     = "./logspath",

    dbtype       = "mysql",
    dbname       = "pgu235_dt",
    host         = "localhost",
    user         = "root",
    passwd       = "54321"
)
