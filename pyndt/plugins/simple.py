#!/usr/bin/env python
# coding=utf-8
# Stan June 18, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

"""
*** Плагин для оболочки ***
Пример плагина
"""


print("Обычный скрипт, выполняемый в консоли")


def Run():
    print("Действие, выполняемое при запуске модуля, дополнительно к \
основному коду.")


if __name__ == '__main__':
    Run()
