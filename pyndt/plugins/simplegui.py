#!/usr/bin/env python
# coding=utf-8
# Stan July 06, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

"""
*** Плагин для оболочки ***
Пример GUI скрипта, который может запускаться из под нашей программы
"""

import os, sys
import wx                       # GUI


if __name__ == '__main__':
    app = wx.App()


frame = wx.Frame(None, -1, "Simple GUI Example")
wx.Button(frame, -1, label = "Start")
frame.Show(True)


def Run():
    print("Действие, выполняемое при запуске модуля, дополнительно к \
основному коду.")


def main():
    app.MainLoop()


if __name__ == '__main__':
    main()
