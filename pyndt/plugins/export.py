#!/usr/bin/env python
# coding=utf-8
# Stan July 30, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

""" *** Плагин для оболочки ***
Экспорт xls-файлов из папки ndt_xls_path в базу данных ndt_dbname/ndt_dbname
"""

import sys, os, fnmatch, time
import wx                       # GUI

from lib.settings import Settings
import lib.xls_export       as  xls_export      # Распознавание таблиц из xls-файла
import lib.xls_treelist     as  xls_treelist    # Графическое отображение распознанного
import lib.xls2db           as  xls2db          # Запись в базу данных


# Загружаем настройки
s = Settings()
export_conf = s.get_group("EXPORT")
export_defs = s.get_group("defs")


class Frame(wx.Frame):
    def __init__(self, parent=None):
        pos = (20, 20)
        if parent:
            pos = parent.GetPosition() + pos
        wx.Frame.__init__(self, None, -1, "Export XLS", pos = pos, size = (800, 600))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.cTreeList = xls_treelist.reportsTreelist(self)
        sizer.Add(self.cTreeList, proportion = 1, flag = wx.GROW)
        if self.cTreeList:
            self.cTreeList.DeleteChildren(self.cTreeList.root)
#       self.SetSizerAndFit(sizer)
        self.SetSizer(sizer)


# Если передан элемент cTreeList - то создаём на нём дерево
def Proceed():
    # Проверяем директорию
    export_path = export_conf.get('ndt_xls_path')
    if not os.path.isdir(export_path):
        wx.MessageBox("Данной директории не существует: %s" % export_path, "Error")
        return

    # Вычисляем время выполнения
    a1 = time.time()

    # Открываем базу данных
    cDB = xls2db.db(export_conf)
    if not cDB.db.conn:
        wx.MessageBox("Невозможно установить соединение с базой данных")
        return

    dbtype = export_conf.get('dbtype')
    dbname = export_conf.get('dbname')
    print("Indexing '{0}'' into {1}:{2}".format(export_path, dbtype, dbname))

    citem = frame.cTreeList.root
    [dir_reports, dir_save, dir_errors, dir_warnings] = walktree(export_path, cDB, frame.cTreeList, citem)

    print("%4u /%4u; %3u; %3u/" % (dir_reports, dir_save, dir_errors, dir_warnings))
    cDB.db.commit()

    a2 = time.time()
    d = int(a2 - a1)
    min = int(d / 60)
    sec = d % 60
    print("Общее время распознавания: {0} мин {1} сек".format(min, sec))

    frame.cTreeList.Expand(citem)       # раскрываем корень
    return 1


# Функция принимает в качестве параметра полный путь к каталогу и полностью просматривает его
# на наличие xls-файлов.
# Если задан параметр citem (графический режим), то выстраивает найденные папки и файлы
# в дерево citem (item of TreeListCtrl)
def walktree(fullpath, cDB, cTreeList=None, citem=None):
    dir_reports  = 0
    dir_save     = 0
    dir_errors   = 0
    dir_warnings = 0

    citem_dir = citem_file = None

    # Сначала проходим директории
    ldir = sorted(os.listdir(fullpath))
    for basename in ldir:
        filename = os.path.join(fullpath, basename)
        if os.path.isdir(filename):
            basename = os.path.basename(filename)
#           print(filename, end=' ')
            print(filename.encode('cp1251', 'ignore'), end=' ')
            if basename != '.svn' and basename[0] != '_' and basename[-1] != '_':
                print()
                if cTreeList:
                    citem_dir = cTreeList.AppendItem(citem, basename)
                    cTreeList.SetItemImage(citem_dir, cTreeList.fldridx,     which = wx.TreeItemIcon_Normal )
                    cTreeList.SetItemImage(citem_dir, cTreeList.fldropenidx, which = wx.TreeItemIcon_Expanded)
                    cTreeList.SetItemBackgroundColour(citem_dir, wx.LIGHT_GREY)
                    cTreeList.SetItemBold(citem_dir)
                sub_reports, sub_save, sub_errors, sub_warnings = walktree(filename, cDB, cTreeList, citem_dir)

                dir_reports  += sub_reports
                dir_save     += sub_save
                dir_errors   += sub_errors
                dir_warnings += sub_warnings
            else:
                print("_skipped_", end='')
                print()

    # Теперь выводим файлы
    ldir = sorted(os.listdir(fullpath))
    for basename in ldir:
        filename = os.path.join(fullpath, basename)
        if os.path.isfile(filename) and fnmatch.fnmatch(filename, '*.xls'):
            basename = os.path.basename(filename)
# Начало обработки файла
            if basename[0] != '_':
                if cTreeList:
                    citem_file = cTreeList.AppendItem(citem, basename)
                    cTreeList.SetItemImage(citem_file, cTreeList.fileidx, which = wx.TreeItemIcon_Normal)
                    cTreeList.SetItemBold(citem_file)
                sheets1, reports1, save1, errors1, warnings1, error_list1 = xls_export.export_file(filename, export_conf, export_defs, cDB, cTreeList, citem_file)
#                 sheets1, reports1, save1, errors1, warnings1, error_list1 = 0, 0, 0, 0, 0, []

                print("%3u: %3u <%3u %2u %2u> | " % (sheets1, reports1, save1, errors1, warnings1), end='')
                dir_reports  += reports1
                dir_save     += save1
                dir_errors   += errors1
                dir_warnings += warnings1
#                 if error_list1:
#                     error_list.append([basename, error_list1])
#               os.utime(filename, (lasttime, lasttime))
            else:
                print("_skip_", end='')

#           print(basename)
            print(basename.encode('cp1251', 'ignore'))

    print()
    # Если директория распозналась с ошибками или замечаниями, то раскрываем эту ветвь
    if cTreeList:
        if dir_errors or dir_warnings:
            cTreeList.Expand(citem)
    return dir_reports, dir_save, dir_errors, dir_warnings


if __name__ == '__main__':
    app = wx.App()


frame = Frame(None)
if Proceed():
    frame.Show(True)


def main():
    app.MainLoop()


if __name__ == '__main__':
    main()
