#!/usr/bin/env python
# coding=utf-8
# Stan 2008-02-04

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import sys, os, logging
import wx                       # GUI

import lib.frames as frames     # Функция для загрузки главного окна программы


def main(args=None):
    app = wx.App()              # Приложение
    app.mf = frames.Show("mainframe")

#   import wx.py.filling as filling
#   filling.FillingFrame(None, rootLabel="app", rootObject=app).Show()
    app.MainLoop()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    sys.exit(main())
