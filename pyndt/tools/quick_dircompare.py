#!/usr/bin/env python
# coding=utf-8
# Stan November 18, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import os, sys

import lib.walktree             # Просмотр директории


""""""""""""""""""""""""""""""""""""
""" Интерфейс функции proceed    """
""""""""""""""""""""""""""""""""""""

def dir_func(filename, basename, relname, cookies, level):
    pass

def file_func(filename, basename, relname, cookies):
#     try:
        statinfo1 = os.stat(filename)
        fullname2 = os.path.join(dir2, relname)
        if os.path.isfile(fullname2):
            statinfo2 = os.stat(fullname2)
            if statinfo1.st_size != statinfo2.st_size:
                print("Размер различается: ", filename, statinfo1.st_size, statinfo2.st_size)
            if statinfo1.st_mtime != statinfo2.st_mtime:
                print("Время различается: ", filename, statinfo1.st_mtime, statinfo2.st_mtime)
        else:
            print("Файл отсутствует:", fullname2)
#     except Exception as e:
#         print("===", repr(filename), "===")


dir1 = "x:\\"
dir2 = "z:\\"
lib.walktree.proceed(dir1, dir_func, file_func)

print("--------------------")

dir1, dir2 = dir2, dir1
lib.walktree.proceed(dir1, dir_func, file_func)
