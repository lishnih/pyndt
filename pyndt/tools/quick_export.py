#!/usr/bin/env python
# coding=utf-8
# Stan October 04, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import sys, time
import wx

import lib.walktree             # Просмотр директории
import lib.xls_treelist         # GUI Treelist
import lib.xls_export           # Распознавание заключений
import lib.xls2db               # База данных


""""""""""""""""""""""""""""""""""""
""" WX GUI интерфейс             """
""""""""""""""""""""""""""""""""""""

class Frame(wx.Frame):
    def __init__(self, parent=None):
        pos = (20, 20)
        if parent:
            pos = parent.GetPosition() + pos
        wx.Frame.__init__(self, None, -1, "Export XLS", pos = pos, size = (800, 600))
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.cTreeList = lib.xls_treelist.reportsTreelist(self)
        sizer.Add(self.cTreeList, proportion = 1, flag = wx.GROW)
        if self.cTreeList:
            self.cTreeList.DeleteChildren(self.cTreeList.root)
        self.SetSizer(sizer)


""""""""""""""""""""""""""""""""""""
""" Интерфейс функции proceed    """
""""""""""""""""""""""""""""""""""""

dir_reports  = 0
dir_save     = 0
dir_errors   = 0
dir_warnings = 0

def dir_func(filename, basename, relname, parent_item, level):
    global dir_reports, dir_save, dir_errors, dir_warnings

    dir_reports  = 0
    dir_save     = 0
    dir_errors   = 0
    dir_warnings = 0

    child_item = None
    if parent_item:
        child_item = cTreeList.AppendItem(parent_item, basename)
        cTreeList.SetItemImage(child_item, cTreeList.fldridx,     which = wx.TreeItemIcon_Normal)
        cTreeList.SetItemImage(child_item, cTreeList.fldropenidx, which = wx.TreeItemIcon_Expanded)
        cTreeList.SetItemBackgroundColour(child_item, wx.LIGHT_GREY)
        cTreeList.SetItemBold(child_item)
    o.write(".")
    return child_item

def file_func(filename, basename, relname, parent_item):
    global dir_reports, dir_save, dir_errors, dir_warnings

    if basename[0] != '_':
        if parent_item:
            file_item = cTreeList.AppendItem(parent_item, basename)
            cTreeList.SetItemImage(file_item, cTreeList.fileidx, which = wx.TreeItemIcon_Normal)
            cTreeList.SetItemBold(file_item)

        [sheets1, reports1, save1, errors1, warnings1, error_list1, lasttime] = lib.xls_export.export_file(filename, conf, defs, cDB, cTreeList, file_item)
        # doc-файлы выдают ошибку
        dir_reports  += reports1
        dir_save     += save1
        dir_errors   += errors1
        dir_warnings += warnings1

# def dir_out_func():
#     if dir_errors or dir_warnings:
#         cTreeList.Expand(citem)

# Приложение
app = wx.App()

# База данных
cDB = lib.xls2db.db()       # !!! ошибка, если mysql не установлен
if not cDB.db.conn:
    wx.MessageBox("Невозможно установить соединение с базой данных")
    exit()

# GUI
frame = Frame(None)
cTreeList = frame.cTreeList
root = cTreeList.root

o = sys.stdout

a1 = time.time()
xls_dir = "D:\\home\\stan\\ndt\\reports"
lib.walktree.proceed(xls_dir, dir_func, file_func, root) # !!! нет проверки на существующую директорию

# print("%4u /%4u; %3u; %3u/" % (dir_reports, dir_save, dir_errors, dir_warnings))
a2 = time.time()
d = int(a2 - a1)
min = int(d / 60)
sec = d % 60
print("Общее время распознавания: %u мин %u сек" % (min, sec))

cTreeList.Expand(root)      # раскрываем корень
frame.Show(True)

def main():
    app.MainLoop()

if __name__ == '__main__':
    main()
