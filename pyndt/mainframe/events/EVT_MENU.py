# coding=utf-8
# Stan July 11, 2008

import logging


def onEvent(evt):
    logging.info("type: %s; id: %s" % (evt.GetEventType(), evt.GetId()))
