# coding=utf-8
# Stan July 11, 2008

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    ��� ���� �� �������� � wxPython
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

import wx


# Activate the widget inspection tool
from wx.lib.inspection import InspectionTool
if not InspectionTool().initialized:
    InspectionTool().Init()

def onEvent(evt):

    # Find a widget to be selected in the tree.  Use either the
    # one under the cursor, if any, or this frame.
    wnd = wx.FindWindowAtPointer()
    if not wnd:
        wnd = self
    InspectionTool().Show(wnd, True)
