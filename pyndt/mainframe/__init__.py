# coding=utf-8
# Stan July 09, 2008

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

"""
Главный фрейм программы, задаёт всю логику
Этот фрейм загружается модулем app.frames, поэтому отвечает
определённым требованиям:
- класс называется Frame
- __init__() содержит две инструкции:
    self.this = frame.this
    self.thisown = frame.thisown
"""

import os, glob, logging
import wx, wx.xrc, wx.html
from wx.py.filling import Filling   # Отладка

from lib.settings import Settings
import lib.frames as frames         # Фреймы
import lib.modplugs as modplugs     # Плагины


plugs = {}      # Массив плагинов


# Инициализация настроек
s = Settings()
s.saveEnv()


class Frame(frames.Frame):
    def __init__(self, framedir, options=None):
        frames.Frame.__init__(self, framedir, options)

        # Notebook
        self.nb = wx.xrc.XRCCTRL(self, 'ID_NOTEBOOK')
        self.nb_description = wx.xrc.XRCCTRL(self, 'ID_HTML_DESCRIPTION')
        self.nb_config      = wx.xrc.XRCCTRL(self, 'ID_CONFIG')
        self.nb_output      = wx.xrc.XRCCTRL(self, 'ID_HTML_OUTPUT')

        pluginspath = s.get_path('pluginspath', check=True)
        self.WalkPlugins(pluginspath)     # просматривает директорию с плагинами
        self.current = None               # текущий плагин

        # Отладочный pyFilling
        try:
            filling = Filling(parent=self.nb)
            self.nb.AddPage(page=filling, text='Namespace')
        except Exception as e:
            pass


    def WalkPlugins(self, pluginspath):
        filemask = os.path.join(pluginspath, '*.py')
        ldir = sorted(glob.glob(filemask))
        modulename_list = []
        for filename in ldir:
            if os.path.isfile(filename):
                basename = os.path.basename(filename)
                if basename[0] != '_':
                    plugs[basename] = modplugs.Load(filename)
                    modulename_list.append(basename)

        listctrl = wx.xrc.XRCCTRL(self, 'ID_LISTBOX')
        listctrl.Set(modulename_list)


    def ShowDescription(self):
        self.nb.SetSelection(0)


    def RenewDescription(self, basename):
        str = plugs[basename].GetDescription()
        if str:
            self.nb_description.SetPage(str)
        else:
            self.nb_description.SetPage("<html><body>Нет описания!</body></html>")


    def ShowParams(self):
        self.nb.SetSelection(1)


    def RenewParams(self, basename):
        res = plugs[basename].GetResource()
        if res:
            res.LoadPanel(self.nb_config, 'ID_PANEL')
        else:
            self.nb_config.DestroyChildren()


    def Run(self, basename):
        self.nb.SetSelection(2)
        self.nb_output.SetPage("")
        plugs[basename].Start()
