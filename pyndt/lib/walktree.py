#!/usr/bin/env python
# coding=utf-8
# Stan October 04, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import os, glob


# Пролистывает директорию dirname
# dir_func      callback-функция, когда встречена директория
# file_func     callback-функция, когда встречен файл
# cookies       кукисы
# home          для внутреннего использования
# level         для внутреннего использования
def proceed(dirname, dir_func, file_func=None, cookies=None, home=None, level=0):
    if home == None:
        home = dirname

    # Сначала проходим директории
    ldir = sorted(os.listdir(dirname))
    for basename in ldir:
        filename = os.path.join(dirname, basename)
        relname  = os.path.relpath(filename, home)
        if os.path.isdir(filename):
            if basename[0] != "." and basename[0] != "_" and basename[-1] != "_":
                new_cookies = dir_func(filename, basename, relname, cookies, level)
                proceed(filename, dir_func, file_func, new_cookies, home, level+1)

    # Теперь файлы
    if file_func:
        filemask = os.path.join(dirname, "*.xls")
        ldir = sorted(glob.glob(filemask))
        for filename in ldir:
            if os.path.isfile(filename):
                basename = os.path.basename(filename)
                relname  = os.path.relpath(filename, home)
                file_func(filename, basename, relname, cookies)
