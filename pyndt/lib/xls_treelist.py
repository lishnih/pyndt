#!/usr/bin/env python
# coding=utf-8
# Stan September 23, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import os
import wx
import wx.gizmos as gizmos      # treelist с колонками

import res.images as images


class reportsTreelist(wx.gizmos.TreeListCtrl):
    def __init__(self, frame):
        gizmos.TreeListCtrl.__init__(self, frame, style = wx.TR_DEFAULT_STYLE | wx.TR_FULL_ROW_HIGHLIGHT)

        # Загружаем иконки
        isz = (16,16)
        il = wx.ImageList(isz[0], isz[1])
        self.fldridx     = il.Add(wx.ArtProvider_GetBitmap(wx.ART_FOLDER,      wx.ART_OTHER, isz))
        self.fldropenidx = il.Add(wx.ArtProvider_GetBitmap(wx.ART_FILE_OPEN,   wx.ART_OTHER, isz))
        self.fileidx     = il.Add(wx.ArtProvider_GetBitmap(wx.ART_NORMAL_FILE, wx.ART_OTHER, isz))
        self.smileidx    = il.Add(images.getSmilesBitmap())
        self.SetImageList(il)
        self.il = il

        # Добавляем колонки
        self.AddColumn("Dirs / Files / Sheets")
        self.AddColumn("Type")
        self.AddColumn("Reports")
        self.AddColumn("Joints")
        self.AddColumn("Errors")
        self.SetMainColumn(0)
        self.SetColumnWidth(0, 350)

        # добавляем корень
        self.root = self.AddRoot('...')
#         self.SetToolTip("???", self.root)
        self.activated = 0

        # список событий
        self.GetMainWindow().Bind(wx.EVT_RIGHT_UP, self.OnRightUp)


#   def __del__(self):
#       print("Удаление объекта:", self)


    def Activate(self):
        err_code = Export_XLS(glov.ndt_xls_path, self)
        return err_code


    def Setitem(self, citem, *args):
        i = 1
        for str in args:
            str = unicode(str)
            self.SetItemText(citem, str, i)
            i += 1


    def Appendmix(self, citem, cmix):
        if cmix:
            if isinstance(cmix, list):
                for str in cmix:
                    self.AppendItem(citem, str)
            elif isinstance(cmix, dict):
                keys = cmix.keys()
                keys.sort()
                for key in keys:
                    citem2 = self.AppendItem(citem, key)
                    self.Setitem(citem2, cmix[key])
            else:
                self.AppendItem(citem, cmix)


    def Appenderrors(self, citem, cerrors):
        if cerrors:
            if isinstance(cerrors, list):
                for str in cerrors:
                    citem2 = self.AppendItem(citem, str)
                    self.SetItemTextColour(citem2, wx.RED)
            elif isinstance(cerrors, dict):
                keys = citem.keys()
                for key in keys:
                    citem2 = self.AppendItem(citem, key)
                    self.Setitem(citem2, citem[key])
                    self.SetItemTextColour(citem2, wx.RED)
            else:
                citem2 = self.AppendItem(citem, cerrors)
                self.SetItemTextColour(citem2, wx.RED)


    def Appendwarnings(self, citem, cerrors):
        if cerrors:
            if isinstance(cerrors, list):
                for str in cerrors:
                    citem2 = self.AppendItem(citem, str)
                    self.SetItemTextColour(citem2, wx.CYAN)
            elif isinstance(cerrors, dict):
                keys = citem.keys()
                for key in keys:
                    citem2 = self.AppendItem(citem, key)
                    self.Setitem(citem2, citem[key])
                    self.SetItemTextColour(citem2, wx.CYAN)
            else:
                citem2 = self.AppendItem(citem, cerrors)
                self.SetItemTextColour(citem2, wx.CYAN)


    def OnRightUp(self, evt):
        pos = evt.GetPosition()
        citem, flags, col = self.HitTest(pos)
        if citem:
            itext = self.GetItemText(citem, 0)
            print('Flags: %s, Col:%s, Text: %s' % (flags, col, itext))
            names = []
            while 1:
                # Если добрались до головного элемента, то выходим
                if self.GetRootItem() == citem:
                    break
                up = self.GetItemParent(citem)
                sub = self.GetItemText(up)
                names.insert(0, sub)
                citem = up
            str = ""
            for val in names:
                str = os.path.join(str, val)
            if str[-4:] == ".xls":
                print(str, itext)
#             func.getpath(self, citem)
#             croot = self.GetRootItem()
#             print(self.GetItemText(croot, 0))
#             recog.showsheet(self.GetItemText(item, 0))
