#!/usr/bin/env python
# coding=utf-8
# Stan September 23, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )
from lib.backwardcompat import *

import os, re, logging
import wx
import xlrd                         # Чтение таблиц xls

from lib.settings import Settings
import lib.lxls as lxls             # дополнение к xlrd
import lib.xls_report as xls_report # функции распознавания отчётов
import lib.xls_joints as xls_joints # функции распознавания стыков

from lib.xls_joints import fields


defs_dict = {}


# Функция принимает в качестве параметра полный путь к xls-файлу, производит чтение
# и распознавание данных из него.
def export_file(filename, conf, defs, cDB, cTreeList=None, citem_file=None):
    filetype        = None              # тип отчётов в файле
    file_reports    = 0                 # кол-во отчётов (для которых тип определён)
    file_save       = 0                 # кол-во сохранённых отчётов
    file_errors     = 0                 # кол-во отчётов в которых возникли ошибки
    file_warnings   = 0                 # кол-во отчётов в которых возникли ошибки
    file_error_list = []                # список ошибок по листам
    lasttime        = 0                 # дата последнего заключения

    # Открываем файл
    book = xlrd.open_workbook(filename)
    file_sheets = book.nsheets          # кол-во таблиц в файле
    filename = filename.replace("\\", "\\\\")

    # Цикл по всем листам файла
    citem_sheet  = None                 # ещё не объявлен при первом проходе
    error_list   = []                   # список ошибок на листе
    warning_list = []                   # список замечаний на листе
    for i in range(book.nsheets + 1):
        # В начале цикла добавляются данные из предыдущего цикла
        # Из-за этого циклов на 1 больше, чем кол-во листов
        if citem_sheet:
            cTreeList.Setitem(citem_sheet, form_str, reports_str, joints_str, errors_str)
            cTreeList.Appenderrors(citem_sheet, error_list)
            cTreeList.Appendwarnings(citem_sheet, warning_list)
            cTreeList.Appendmix(citem_sheet, report_dict)
            for joint_dict in joint_list:
                citem_joint = cTreeList.AppendItem(citem_sheet, "%s [%r]" % (joint_dict['joint_str'], joint_dict['joint_str']))
                cTreeList.Appendmix(citem_joint, joint_dict)
        # Если в прошлом цикле возникли ошибки, считаем и запоминаем их
        if error_list:
            file_errors += 1
            file_error_list += [shname, error_list]
        # Если в прошлом цикле возникли замечания, считаем и отмечаем их
        if warning_list:
            file_warnings += 1
            if cTreeList and not error_list:
                cTreeList.SetItemTextColour(citem_sheet, wx.CYAN)

        report_dict = {}            # данные извлечённые из отчёта
        report_prop = {}            # характеристики отчёта
        joint_list  = []            # массив стыков в отчёте
        form_str    = ""            # метки для таблицы распознавания
        reports_str = ""            # далее текстовые метки для cTreeList
        joints_str  = ""
        errors_str  = ""

        # Начинаем основной цикл по таблицам файла
        if i < book.nsheets:
            error_list   = []           # обнуляем список ошибок
            warning_list = []           # обнуляем список замечаний

            sh = book.sheet_by_index(i)
            shname = sh.name
            if cTreeList:
                citem_sheet = cTreeList.AppendItem(citem_file, shname)
            if shname[0] == '_':        # Если лист закомментирован
                if cTreeList:
                    cTreeList.Setitem(citem_sheet, "_")
                continue
            elif shname[0] == '-':      # Если лист удалён, то помечаем его удалённым
                enabled = 0
            else:                       # Иначе отчёт активен
                enabled = 1

            # Началось распознавание - окрашиваем в красный цвет
            if cTreeList:
                cTreeList.SetItemTextColour(citem_sheet, wx.RED)
################################
# Секция распознания отчёта
################################
            if not sh.ncols and not sh.nrows:
                continue
#             if sh.visibility:
#                 continue
            try:
                [def_obj, report_dict, error_list1, warning_list1] = recognize_report(sh, conf, defs)
                error_list   += error_list1         # Заносим ошибки, если они есть
                warning_list += warning_list1       # Заносим замечания, если они есть
            except Exception as e:
                print("[[[[[[[[[[")
                print("Ошибка в файле/листе при распознавании заключения:", filename, shname)
                print()
                logging.exception(e)
                print("                              ]]]]]]]]]]\n")
                continue  # exit()

            # Если тип отчёта не определился, то прерываем процесс распознавания
            if def_obj == None:
                continue

            report_dict['enabled'] = enabled        # Заносим в словарь
            report_dict['filename'] = filename      # Запоминаем имя файла

            if lasttime < report_dict['date']:
                lasttime = report_dict['date']
            file_reports += 1

################################
# Секция распознания стыков
################################
            try:
                [kp, joints, joint_list, error_list1, warning_list1] = recognize_joints(sh, def_obj, report_dict)
                error_list   += error_list1         # Заносим ошибки, если они есть
                warning_list += warning_list1       # Заносим замечания, если они есть
            except Exception as e:
                print("[[[[[[[[[[")
                print("Ошибка в файле/листе при распознавании стыков:", filename, shname)
                print()
                logging.exception(e)
                print("                              ]]]]]]]]]]\n")
                continue  # exit()

            if joint_list:
                report_dict['joints'] = joints
                joints_str = str(joints)
                if kp:
                    report_dict['kp'] = kp
            else:
                error_list.append("Не найдено ни одного стыка")

            # Если возникли хоть какие-то ошибки, то прерываем цикл
            if error_list:
                continue

            # Распознание прошло успешно - окрашиваем в серый цвет
            if cTreeList:
                cTreeList.SetItemTextColour(citem_sheet, wx.LIGHT_GREY)
################################
# Секция сохранения отчёта
################################
            [rep_id, error_str] = cDB.save_report(def_obj, report_dict)
            if error_str:
                error_list.append(error_str)
                continue

            if rep_id == None:
                continue
            else:
                file_save += 1
                if cTreeList:
                    cTreeList.Appendmix(citem_sheet, {"Номер записи": rep_id})
################################
# Секция сохранения стыков
################################
            [i, j, error_list1] = save_joints(cDB, def_obj, rep_id, joint_list)
            if joints > i:
                error_list.append("Сохранено стыков: %i; Не сохранено: %i" % (i, j))
                error_list += error_list1
                continue
################################
            # Запись прошла успешно - окрашиваем в синий цвет
            if cTreeList:
                cTreeList.SetItemTextColour(citem_sheet, wx.BLUE)
################################
    if cTreeList:
        if filetype != None:
            cTreeList.Setitem(citem_file, filetype, file_reports, "", file_errors)
        if file_save:
            cTreeList.SetItemTextColour(citem_file, wx.BLUE)
        if file_warnings:
            cTreeList.SetItemTextColour(citem_file, wx.CYAN)
        if file_errors:
            cTreeList.SetItemTextColour(citem_file, wx.RED)
    return file_sheets, file_reports, file_save, file_errors, file_warnings, file_error_list


# Данная функция готовит словарь по списку ключей
def prepare_dict(field_list):
    d = {}
    for field in field_list:
        d[field] = ""
    return d


def recognize_report(sh, conf, defs):
    def_obj = None
    report_dict  = prepare_dict(fields['reports'])
    error_list   = []
    warning_list = []

    # Настройки по умолчанию
    def_default = conf.get("def_default")
    # def_func = conf.get("def_func")

    # Тип заключения
    def_id = def_default
    val = lxls.get_value(sh, 0, 0)
    if val and isinstance(val, string_types) and val[0] == '{':
        res = re.match(r"{def: ?(.+); ?rev: ?(\d+)(?:;.+)?}", val)
        if res:
            reslist = res.groups()
            def_id  = reslist[0]
            rev     = int(reslist[1])
        else:
            warning_list.append("Проверьте форму распознаваемого файла")
    else:
        warning_list.append("Тип распознаваемого файла не задан!")

    if not def_id:
        return [def_obj, report_dict, error_list, warning_list]

    if def_id not in defs_dict:
        def_obj = defs.get_group(def_id)
        if not def_obj:
            error_list.append("Тип отчёта [%s] не описан!" % def_id)
            return [def_obj, report_dict, error_list, warning_list]

        defs_dict[def_id] = def_obj
    else:
        def_obj = defs_dict[def_id]

    report_dict['def_id'] = def_id

    xls_type = def_obj.get('xls_type')
    if xls_type:
        report_dict['method'] = xls_type.get('method')

    params = def_obj.get('xls_formsearch')
    if params:
        for cur in params:
#           print(0, params[cur])
            type_s = lxls.search_hor_value(sh, params[cur])
#           print(1, type_s)

            if isinstance(type_s, list):
                y0, x0, found = type_s
#               revision = "auto%02u%02u" % (x0, y0)
                str1 = found[0]
                if isinstance(str1, float):
                    str1 = str(int(str1))
                report_dict[cur] = str1
            else:
                warning_list.append("Не найден: %s '%s'" % (cur, type_s))

        # Дата
        try:
            [revision, report] = xls_report.recognize_report_date(sh, params)
            if revision == None:
                warning_list.append("Дата не распознана")
            else:
                report_dict.update(report)
        except Exception as e:
            warning_list.append("Ошибка в листе при распознавании даты!")

        if not report_dict['date']:
            warning_list.append("Дата не определена")

        # Номер отчёта
        [revision, report] = xls_report.recognize_report_id(sh, params)
        if revision == None:
            error_list.append("Номер отчёта не распознан")
        else:
            report_dict.update(report)

    return [def_obj, report_dict, error_list, warning_list]


def recognize_joints(sh, def_obj, report_dict):
    rep_kp       = 0
    rep_diameter = 0
    rep_joints   = 0
    joint_list   = []
    error_list   = []
    warning_list = []
    shname = sh.name

    xls_linesearch = def_obj.get('xls_linesearch')
    if xls_linesearch:
        startline, stopline, start_offset = xls_linesearch

        if isinstance(startline, int):
            start = startline
            stop = stopline
        else:
            xy = lxls.search_value(sh, startline)
            if xy:
                start = xy[0]
                xy = lxls.search_value(sh, stopline)
                if xy:
                    stop = xy[0]
                else:
                    error_list.append("Стоп строка не найдена")
                    return [rep_diameter, rep_joints, joint_list, error_list, warning_list]
            else:
                error_list.append("Старт строка не найдена")
                return [rep_diameter, rep_joints, joint_list, error_list, warning_list]

        for j in range(start + start_offset, stop):
            [joint_dict, error_list1, warning_list1] = xls_joints.recognize_joint(sh, def_obj, j)
            error_list   += error_list1
            warning_list += warning_list1

            if joint_dict and joint_dict['kind']:
                rep_joints += 1

                if joint_dict['kp'] != None:        # Пикет
                    # Проверяем, чтобы все отчёты в файле были на одном километре
                    kp = joint_dict['kp']
                    if rep_kp == 0:
                        rep_kp = kp
                if rep_kp > 0 and kp != rep_kp:
                    err_str = "Разные километры [%u]" % (j + 1)
                    warning_list.append(err_str)
                    rep_kp = -1

                joint_list.append(joint_dict)

    return [rep_kp, rep_joints, joint_list, error_list, warning_list]


def save_joints(cDB, def_obj, rep_id, joint_list):
    i = 0               # число удачно сохранённых стыков
    j = 0               # число неудачно сохранённых стыков
    error_list = []     # список несостоявшихся запросов
    for joint_dict in joint_list:
        error_query = cDB.save_joint(def_obj, rep_id, joint_dict)
        if error_query:
            j += 1
            error_list.append(error_query)
        else:
            i += 1

    return [i, j, error_list]
