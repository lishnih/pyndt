#!/usr/bin/env python
# coding=utf-8
# Stan September 27, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

"""
Интерфейс XLS_Export для работы БД MySQL
"""

import MySQLdb as mysql         # MySQL


class db:
    def __init__(self, mysql_host, mysql_user, mysql_pass):
        self.mysql_host = mysql_host
        self.mysql_user = mysql_user
        self.mysql_pass = mysql_pass
        try:
            self.conn = mysql.connect(mysql_host, mysql_user, mysql_pass)
            self.conn.set_character_set('utf8')
        except Exception as e:
            self.conn = None


    def open(self, dbname):
#       self.dbname = dbname
#       self.execute("CREATE DATABASE IF NOT EXISTS %s" % dbname)
        self.cur = self.conn.cursor()
        self.conn.select_db(dbname)


    def new(self, dbname):
        self.open(dbname)
        try:
            self.execute("DROP TABLE reports,joints,entries")
        except Exception as e:
            pass


    # Выполняем запрос
    def execute(self, sql):
        try:
            return self.cur.execute(sql)
        except Exception as e:
            print(e, sql)


    # Добавляем запись
    def insert(self, sql):
        self.execute(sql)
        id = self.cur.lastrowid
        return id


    def commit(self):
        self.conn.commit()


    def close(self):
        self.commit()
        self.cur.close()
        self.conn.close()
