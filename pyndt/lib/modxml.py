#!/usr/bin/env python
# coding=utf-8
# stan July 01, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

""" Загрузка и обработка файла XML
Распознаются следующие теги
string      переменная с именем name
            распознаётся потомок parent и сохраняется в переменной
            self.data[parent][name]
            если задан язык ru, то читает переменные указанного языка
resource    ресурс с именем name
            аналогично string, но без языка
"""

import logging
import wx
from xml.dom.minidom import parse

from gettext import gettext as _        # i18n


def CreateXml(filename, lang=""):
    if not lang:
        locale = wx.Locale(wx.LANGUAGE_DEFAULT)
        lang = locale.GetCanonicalName()[0:2]
    m = modXML(filename, lang)
    return m


class modXML():
    def __init__(self, filename, lang=""):
        self._obj     = parse(filename)
        self.filename = filename
        self.lang     = lang
        self.data     = {}
        self.res      = {}

        # string
        elements = self._obj.getElementsByTagName("string")
        for element in elements:
            parent = element.parentNode.localName
            if parent not in self.data:
                self.data[parent] = {}
            name = element.getAttribute("name")
            val  = element.childNodes[0].wholeText
            lang = element.getAttribute("lang")
            if lang == self.lang:
                self.data[parent][name] = val
            elif not name in self.data[parent]:
                self.data[parent][name] = val

        # resource
        elements = self._obj.getElementsByTagName("resource")
        for element in elements:
            parent = element.parentNode.localName
            if parent not in self.res:
                self.res[parent] = {}
            for child in element.childNodes:
                if child.nodeName == "object":
                    str = r"""<?xml version="1.0"?>
<resource>
  %s
</resource>
""" % child.toxml()
#                   self.res[parent] = wx.xrc.EmptyXmlResource()
#                   self.res[parent].LoadFromString(str)
                    wx.FileSystem_AddHandler(wx.MemoryFSHandler())
                    wx.MemoryFSHandler_AddFile("XRC_Resources/xrc_%s" % parent, str)
                    self.res[parent] = wx.xrc.XmlResource("memory:XRC_Resources/xrc_%s" % parent)
                    # отладка
#                   import lib.wxlog
#                   lib.wxlog.print_l(str)
