#!/usr/bin/env python
# coding=utf-8
# Stan July 12, 2008

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

""" Загрузка модулей в переменную

mod_import(filename, name=None, refpath=None)
filename    местонахождение модуля
name        присваиваемое имя модуля
refpath     по этому пути будет искаться filename
"""

import os, imp, logging

import gettext
_ = gettext.gettext


def mod_import(filename, name=None, refpath=None):
    if not name:
        drive, name = os.path.splitdrive(filename)
        components = name.split(os.sep)
        if not components[0]:
            components = components[1:]
        name = '_'.join(components)
        name = name.replace(" ", "_")

    if refpath:
        refpath = os.path.abspath(refpath)
        if not os.path.isabs(filename):
            filename = os.path.join(refpath, filename)

    if os.path.isdir(filename):
        filename = os.path.join(filename, "__init__.py")
    elif os.path.isfile(filename):
        if name[-3:] == ".py":
            name = name[0:-3]
    else:
        logging.warning("Модуль %r не найден!" % filename)
        return None

    fp = open(filename)
    name = name.encode('utf8', 'ignore')
    filename = filename.encode('utf8', 'ignore')
#   print(fp, name, filename)
    try:
        mod = imp.load_source(name, filename, fp)
    except Exception as e:
        mod = None
        logging.exception(e)
    fp.close()
    return mod
