#!/usr/bin/env python
# coding=utf-8
# stan July 6, 2009

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

""" Загрузка плагинов

Load(filename)
filename    имя файла модуля
"""

import os.path, codecs

import lib.modxml as modxml         # Считывание параметров плагинов из xml
import lib.modthreads as modthreads # Каждый плагин запускается в своём потоке


def Load(filename):
    m = modPLUGIN(filename)
    return m


class modPLUGIN():
    def __init__(self, filename):
        self.filename = filename
        xml_filename  = filename[0:-3] + ".xml"
        html_filename = filename[0:-3] + ".html"
        if os.path.isfile(xml_filename):
            self.xmlfile = xml_filename
            self.xml = modxml.CreateXml(xml_filename)
        if os.path.isfile(html_filename):
            self.htmlfile = html_filename

        self._running = 0
        self._th = None

    def isset(self, name):
        return True if name in self.__dict__ else False

    def is_running(self):
        if self._running:
            return True
        else:
            return False

        # !!! app.mt[basename].isAlive() не отображает правильную картину
        if basename in app.mt and app.mt[basename].isAlive():
            print("Модуль {0} уже выполняется!".format(basename))
            return

    def Start(self):
        if not self._running:
            self._running = 1
            self._th = modthreads.Load(self.filename)
            self._th.start()
        else:
            print("Плагин {0} уже выполняется!".format(self.filename))

    def GetDescription(self):
        if self.isset('xml'):
            mdata = self.xml.data['Info']
            return r"""<html>
<head>
 <title></title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf8">
</head>
<body>
<p>%(Description)s</p><br />
<br />
<a href="Run">Выполнить</a><br />
<a href="Params">Конфигурация</a><br />
<hr />
%(Copyright)s
</body>
</html>
""" % mdata
        elif self.isset('htmlfile'):
            return codecs.open(self.htmlfile, "r", "utf8").read()

    def GetResource(self):
        if self.isset('xml'):
            return self.xml.res['Options']
