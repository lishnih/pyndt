#!/usr/bin/env python
# coding=utf-8
# Stan 2007-08-02

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import re

from lib.lxls import *          # Простые функции работы с ячейками


# Classes of joints
float_JOINT = 1

n_JOINT     = 10
x_JOINT     = 20


tables = [
    'reports',    # системные поля: 'id'
    'joints',     # системные поля: 'id'
    'entries'     # системные поля: <нет>
]


fields = {
    tables[0]:   ['enabled', 'method', 'name', 'pre', 'seq', 'rsign', 'q_level', 'tech_map', 'kp', 'date', 'date_str', 'filename', 'joints'],
    tables[1]:   ['kind', 'name', 'type', 'line', 'number', 'sign', 'kp', 'diameter1', 'diameter2', 'thickness1', 'thickness2'],
    tables[2]:   ['_joints_id', '_reports_id', 'decision', 'extras']
}


# Данная функция готовит словарь по списку ключей
def prepare_dict(field_list):
    d = {}
    for field in field_list:
        d[field] = ""
    return d


def get_(dlist, count):
    tlist = []
    for i in range(min(len(dlist), count)):
        tlist.append(dlist[i])

    for j in range(i+1, count):
        tlist.append("")

    return tlist


def recognize_joint_id(str):
    d = dict(name=str)
    d['kind']   = 0
    d['type']   = ""    # префикс
    d['number'] = 0     # номер
    d['sign']   = ""    # суффикс
    d['kp']     = ""    # км

    if isinstance(str, (int, float)):
        d['kind']   = float_JOINT
        d['number'] = str
        return d

#   Signstr_s = r" ?([А-ПС-Я]?[ВЛЗ]?)[- ]?([РрPp]?) *(.*)"
#   Signstr   = r"([Аа]?)-?(ВР\-З|ЛС|ПС|ВР|З|РД|[\w\-]{1,4})-?([РрPp]?) *(.*)"
    Signstr_s = r"(Т?) ?([Rr]?) *(.*)"

    str = str.rstrip()

# КУ2-415-20
    res = re.match(r"([\w.]+)-(.+)-(\d+) *(.*)", str, re.UNICODE)
    if res:
        d['kind'] = n_JOINT
        d['type'], d['line'], d['number'], d['sign'] = res.groups()
        return d

# УПГТ: C2/BD/148
    res = re.match(r"^C2/(\w+)/(\d+)%s$" % Signstr_s, str)
    if res:
        d['kind'] = n_JOINT
        [ d['type'], d['number'], d['sign'], d['signR'], d['signE'] ] = res.groups()
        return d

# УПГТ: Г 1
#     res = re.match(r"^(\w) +(\d+)%s$" % Signstr_s, str, re.UNICODE)
#     if res:
#         d['kind'] = z_JOINT
#         [ d['type'], d['number'], d['signR'], d['signE'] ] = res.groups()
#         return d

    return d


def recognize_thickness(dimthick):
    # Thickness consts
    # Разделители - символ '×', русская 'х' и англ. 'x'
    delimiter_str = '×ХхXx'
    thickness_str = r"([\d.,]+)(?: *[%s] *([Ду]*) *([\d.,]+))?" % delimiter_str
    dimthick_str  = r"([\d.,]+) *[%s] *%s" % (delimiter_str, thickness_str)

    if isinstance(dimthick, float):
        diameter1  = 0
        diameter2  = 0
        thickness1 = dimthick
        thickness2 = 0
        return diameter1, diameter2, thickness1, thickness2

    res = re.match(dimthick_str, dimthick)
    if res:
        diameter1, thickness1, dn, thickness2 = res.groups()
        try:    diameter1  = float(diameter1.replace(",", "."))  if diameter1  else 0.0
        except: diameter1  = -1
        try:    thickness1 = float(thickness1.replace(",", ".")) if thickness1 else 0.0
        except: thickness1 = -1
        if dn:
            try:    diameter2  = float(thickness2.replace(",", ".")) if thickness2 else 0.0
            except: diameter2  = -1
            thickness2 = 0.0
        else:
            try:    thickness2 = float(thickness2.replace(",", ".")) if thickness2 else 0.0
            except: thickness2 = -1
            diameter2  = 0.0
        return diameter1, diameter2, thickness1, thickness2

    return None


def recognize_joint(sh, def_obj, j, gkp = 0):
    joint_list = []
    joint_dict = prepare_dict(fields['joints'])                 # !!!
    joint_dict.update(prepare_dict(fields['entries']))          # !!!
    error_list = []
    warning_list = []
    method = def_obj.get('xls_type')['method']
    xls_namemax = def_obj.get('xls_namemax')
    xls_name_row = def_obj.get('xls_name_row')

    # Если задана колонка наименования стыка name_check
    name_check = def_obj.get('name_check')
    if name_check:
        val = get_value(sh, j, name_check)
        if not val:
            return [joint_dict, error_list, warning_list]

    # Формируем массив значений на стык
    first = 1
    for i in range(sh.ncols):
        # Сканируем только до определённого поля
        if first and i > xls_namemax:           # !!!
            return [joint_dict, error_list, warning_list]

        val = get_value(sh, j, i)
        if val:
            if isinstance(val, basestring):
                val = re.sub(r'([ \n]+)', ' ', val)
            joint_list.append(val)
            first = 0

#     if joint_list == [' ']:
#         return [joint_dict, error_list, warning_list]

    joint_len = len(joint_list)

    if xls_name_row >= joint_len:
        warning_list.append("Неполные данные о стыке: %s" % j)
        return [joint_dict, error_list, warning_list]

    if joint_len:
#       str = get_value(sh, j, xls_name_row)
        str = joint_list[xls_name_row]
        if str:
            if isinstance(val, basestring):
                try:
                    str = re.sub(r'([ \n]+)', ' ', str)
                except Exception as e:
                    print("Исключение в строке: %s" % str)
        else:
            warning_list.append("Невозможно распознать стык данного типа отчёта (отсутствует номер стыка): %s [%s]" % (method, str))
            return [joint_dict, error_list, warning_list]

        if isinstance(str, float):  # Число преобразовываем в строку    # !!!
            joint_dict['joint_str'] = unicode(int(str))
        else:
            joint_dict['joint_str'] = str

        try:
            joint_id = recognize_joint_id(str)
        except Exception as e:
            print("[[[[[[[[[[")
            print("Ошибка при распознавании номера стыка:", str)
            print()
            logging.exception(e)
            print("                              ]]]]]]]]]]\n")
            joint_id = -1
            error_list.append("Ошибка при распознавании номера стыка %s" % str)

        if joint_id == -1:      # если ячейка - не имя стыка
            return [joint_dict, error_list, warning_list]

        joint_dict.update(joint_id)
#         if joint_dict['kp']:
#             joint_dict['kp'] = int(joint_dict['kp'])
#             if gkp and gkp != joint_dict['kp']:
#                 error_list.append("Проверьте км [%02u]:" % j)

        kind = joint_dict['kind']
        if not kind:
            try:
                err = "Стык не распознался[%02u]: %s (%r)" % (j + 1, str, str)
            except Exception as e:
                err = "Стык не распознался[%02u]: %r" % (j + 1, str)
            error_list.append(err)
            return [joint_dict, error_list, warning_list]

####################################
        if method == 'RT':
            if   joint_len < 8:
                error_list.append("Слишком мало данных для стыка: %02u"  % (j + 1))
                return [joint_dict, error_list, warning_list]
            elif joint_len > 16:
                error_list.append("Слишком много данных для стыка: %02u" % (j + 1))
                return [joint_dict, error_list, warning_list]

            dimthick               = joint_list[2]
            joint_dict['welders' ] = joint_list[3]
            joint_dict['decision'] = joint_list[7].strip()
#           joint_dict['remarks' ] = joint_list[8] if joint_len > 8 else ""

            dt = recognize_thickness(dimthick)
            if dt:
                diameter1, diameter2, thickness1, thickness2 = dt
                joint_dict['diameter1' ] = diameter1
                joint_dict['diameter2' ] = diameter2
                joint_dict['thickness1'] = thickness1
                joint_dict['thickness2'] = thickness2
            else:
                try:
                    err = "Диаметр и толщина не определились: %s [%s]" % (str, dimthick)
                except Exception as e:
                    err = "Диаметр и толщина не определились: %s [%r]" % (str, dimthick)
                error_list.append(err)
####################################
        if method == 'VT':
            if   joint_len < 8:
                error_list.append("Слишком мало данных для стыка: %02u"  % (j + 1))
                print(joint_list)
                return [joint_dict, error_list, warning_list]
            elif joint_len > 12:
                error_list.append("Слишком много данных для стыка: %02u" % (j + 1))
                print(joint_list)
                return [joint_dict, error_list, warning_list]

            dimthick               = joint_list[2]
            joint_dict['welders' ] = joint_list[3]
            joint_dict['decision'] = joint_list[7].strip()
#           joint_dict['remarks' ] = joint_list[8] if joint_len > 8 else ""

            dt = recognize_thickness(dimthick)
            if dt:
                diameter1, diameter2, thickness1, thickness2 = dt
                joint_dict['diameter1' ] = diameter1
                joint_dict['diameter2' ] = diameter2
                joint_dict['thickness1'] = thickness1
                joint_dict['thickness2'] = thickness2
            else:
                try:
                    err = "Диаметр и толщина не определились: %s [%s]" % (str, dimthick)
                except Exception as e:
                    err = "Диаметр и толщина не определились: %s [%r]" % (str, dimthick)
                error_list.append(err)
####################################
        if method == 'PT':
            if   joint_len < 8:
                error_list.append("Слишком мало данных для стыка: %02u"  % (j + 1))
                print(joint_list)
                return [joint_dict, error_list, warning_list]
            elif joint_len > 9:
                error_list.append("Слишком много данных для стыка: %02u" % (j + 1))
                print(joint_list)
                return [joint_dict, error_list, warning_list]

            dimthick               = joint_list[2]
            joint_dict['welders' ] = joint_list[3]
            joint_dict['decision'] = joint_list[7].strip()
#           joint_dict['remarks' ] = joint_list[8] if joint_len > 8 else ""

            dt = recognize_thickness(dimthick)
            if dt:
                diameter1, diameter2, thickness1, thickness2 = dt
                joint_dict['diameter1' ] = diameter1
                joint_dict['diameter2' ] = diameter2
                joint_dict['thickness1'] = thickness1
                joint_dict['thickness2'] = thickness2
            else:
                try:
                    err = "Диаметр и толщина не определились: %s [%s]" % (str, dimthick)
                except Exception as e:
                    err = "Диаметр и толщина не определились: %s [%r]" % (str, dimthick)
                error_list.append(err)
####################################
        if method == 'UT':
            if   joint_len < 9:
                error_list.append("Слишком мало данных для стыка: %02u (%02u)"  % (j + 1, joint_len))
                return [joint_dict, error_list, warning_list]
            elif joint_len > 13:
                error_list.append("Слишком много данных для стыка: %02u (%02u)" % (j + 1, joint_len))
                return [joint_dict, error_list, warning_list]

            dimthick                 = joint_list[3]
            joint_dict['welders' ]   = joint_list[5]
#           joint_dict['decision']   = joint_list[8].strip()

            dt = recognize_thickness(dimthick)
            if dt:
                diameter1, diameter2, thickness1, thickness2 = dt
                joint_dict['diameter1' ] = diameter1
                joint_dict['diameter2' ] = diameter2
                joint_dict['thickness1'] = thickness1
                joint_dict['thickness2'] = thickness2
            else:
                try:
                    err = "Диаметр и толщина не определились: %s [%s]" % (str, dimthick)
                except Exception as e:
                    err = "Диаметр и толщина не определились: %s [%r]" % (str, dimthick)
                error_list.append(err)
####################################
        if method == 'Welding':
            if   joint_len < 15:
                error_list.append("Слишком мало данных для стыка: %02u (%02u)"  % (j + 1, joint_len))
                return [joint_dict, error_list, warning_list]
#             elif joint_len > 27:
#                 error_list.append("Слишком много данных для стыка: %02u (%02u)" % (j + 1, joint_len))
#                 return [joint_dict, error_list, warning_list]

#           dimthick               = joint_list[3]
            joint_dict['extras' ]  = joint_list[15]
#           joint_dict['decision'] = joint_list[8].strip()

            joint_dict['diameter1' ] = joint_list[13]
            joint_dict['thickness1'] = joint_list[14]
####################################
    return [joint_dict, error_list, warning_list]
