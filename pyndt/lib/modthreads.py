#!/usr/bin/env python
# coding=utf-8
# stan July 12, 2008

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

""" Загрузка модулей и запуск в отдельном потоке

Load(filename, name)
filename    имя файла модуля
name        имя потока
"""

import threading

import gettext
_ = gettext.gettext

import lib.modules as modules


def Load(filename, name=""):
    t = modTHREAD(filename, name)
    return t


class modTHREAD(threading.Thread):
    def __init__(self, filename, name=""):
        threading.Thread.__init__(self, name=name)
        self.filename = filename
        self.module = modules.mod_import(filename)

    def run(self):
        if "Run" in dir(self.module):
            self.module.Run()
        else:
            print("Модуль {0} не имеет Run".format(self.filename))
