#!/usr/bin/env python
# coding=utf-8
# Stan September 23, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

"""
Интерфейс XLS_Export для работы БД MySQL
"""

import os                       # exists
import sqlite3 as sqlite        # SQLite 3


db_dict = {}    # Здесь хранится список открытых баз данных

class db:
    def __init__(self, sqlite_path):
        self.path = sqlite_path
        self.conn = 1


    def open(self, dbname):
        if dbname in db_dict:
            return db_dict[dbname]
        else:
            self.dbname = dbname
            filename = self.fullname(dbname)
            self.conn = sqlite.connect(filename)
            self.cur = self.conn.cursor()
            db_dict[dbname] = self


    def new(self, dbname):
        self.dbname = dbname
        filename = self.fullname(dbname)

        if dbname in db_dict:
            self.close(dbname)
        if os.path.exists(filename):
            os.remove(filename)

        self.conn = sqlite.connect(filename)
        self.cur = self.conn.cursor()
        db_dict[dbname] = self


    def fullname(self, dbname):
        return os.path.join(self.path, dbname + ".sqlite")


    # Выполняем запрос
    def execute(self, sql):
        try:
            return self.cur.execute(sql)
        except Exception as e:
            print(e, sql)


    # Добавляем запись
    def insert(self, sql):
        self.execute(sql)
        id = self.cur.lastrowid
        return id


    def commit(self):
        self.conn.commit()


    def close(self):
        self.commit()
        self.cur.close()
        self.conn.close()
        del db_dict[self.dbname]
