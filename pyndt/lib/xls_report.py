#!/usr/bin/env python
# coding=utf-8
# Stan August 02, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import re, time

from lib import lxls          # Простые функции работы с ячейками


# sh - лист
# def recognize_report_type(sh):
#     i = 0
#     for xls_type in xls_types:
#         type_s = lxls.search_value(sh, xls_type['search'])
#         if type_s:
#             revision = "t%02u%02u" % (type_s[0], type_s[1])
#             return [revision, i]
#         i += 1
#     return [None, None]


# sh - лист; params - описание параметров
def recognize_report_id(sh, params):
    report = {}
    type_s = lxls.search_hor_value(sh, params['name'])
    if type_s:
        revision = "i%02u%02u" % (type_s[0], type_s[1])
        [report_str] = type_s[2]
        if isinstance(report_str, float):
            report_str = unicode(int(report_str))
        report_str = re.sub(r'([ \n]+)', ' ', report_str)

        # Обрабатываем
        res = re.match(r"([^\d]*) *(\d+) *(.*)", report_str)
        if res:
            reslist = res.groups()
            report['pre']   = reslist[0]
            report['seq']   = reslist[1]
            report['rsign'] = reslist[2]
        return [revision, report]
    return [None, None]


def recognize_report_date(sh, params):
    report = {}
    type_s  = lxls.search_hor_value(sh, params['date_str'])

    if type_s:
        revision = "k%02u%02u" % (type_s[0], type_s[1])
        [date_str] = type_s[2]      # !!! выдаёт ошибку, если неправильно задана start-ячейка
        date = lxls.str_to_date(date_str)

    if not date:
        return [None, None]

    report['date'] = date

    return [revision, report]
