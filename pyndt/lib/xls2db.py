#!/usr/bin/env python
# coding=utf-8
# Stan September 23, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

import os, re, codecs, logging, traceback

from lib.xls_joints import fields


class db:
    def __init__(self, conf, new=True):
        self.conf = conf
        if self.conf.get('dbtype') == "mysql":
            import lib.lmysql as lmysql
            self.db = lmysql.db(self.conf.get('host'), self.conf.get('user'),
                                self.conf.get('passwd'))
        elif self.conf.get('dbtype') == "sqlite":
            import lib.lsqlite as lsqlite
            self.db = lsqlite.db(self.conf.get('path'))

        logging.debug(self.db.conn)
        if self.db.conn:
            if new:
                self.db.new(self.conf.get('dbname'))      # Сбрасываем БД
            else:
                self.db.open(self.conf.get('dbname'))     # Выбираем БД
            self.types = {}                         # будем запоминать создаваемые таблицы
            self.exec_sql_file("", "main")          # Создаём основные таблицы

            self.report_query = self.prep_query("reports", fields['reports'], "id")
            self.joint_query  = self.prep_query("joints",  fields['joints'],  "id")
            self.entry_query  = self.prep_query("entries", fields['entries'])

            logspath = self.conf.get_path('logspath', check=True)
            fname = os.path.join(logspath, "sqls.txt")
            self.f = codecs.open(fname, "w+", "utf8")
        else:
            logging.error("Проверьте правильность настроек MySQL")


    def prep_query(self, table, field_list, index = ""):
        query1 = "INSERT INTO %s (" % table
        query2 = ") VALUES ("
        if index:
            query1 += index
            query2 += "NULL"
            first = 0
        else:
            first = 1
        for field in field_list:
            if first:
                first = 0
            else:
                query1 += ","
                query2 += ","
            query1 += field
            query2 += "'%(" + field + ")s'"
        return query1 + query2 + ")"


    def find_report(self, report_dict):
        sql = "SELECT * FROM reports WHERE enabled='1' AND method='%(method)s' AND pre='%(pre)s' AND seq='%(seq)s' AND rsign='%(rsign)s'" % report_dict
        result = self.db.execute(sql)
        if result:
            row = self.db.cur.fetchall()
            if row:
                return True


    def save_report(self, def_obj, report_dict):
#       print(def_id, report_dict)
        xls_type = def_obj.get('xls_type')
        table = xls_type['table']

        sqldir        = xls_type['domain']
        method_fields = xls_type['fields']

#         if table and table not in self.types:             # Проверяем, создавалась ли такая таблица
#             str = self.exec_sql_file(sqldir, table)
#             if str:
#                 self.types[table] = -2,         # Нет созданной таблицы
#                 return [None, str]
#             else:
#                 method_query = self.prep_query(table, method_fields)
#                 self.types[table] = 1, method_query

        # Если такой отчёт уже есть в таблице, то добавляем @@
        if report_dict['seq'] and report_dict['enabled'] and self.find_report(report_dict):
#           while self.find_report(report_dict):
            report_dict['rsign'] += "@@"

        try:
            sql = self.report_query % report_dict
        except Exception as e:
            print("Sql не подготовлен:", self.report_query, report_dict)
            return [None, "Sql не подготовлен!"]

        try:
            id = self.db.insert(sql)
            return [id, None]
        except Exception as e:
            self.log(sql)
            return [None, "Не выполнилось: %s" % sql]

        return [None, "Что-то не так!"]


    def save_joint(self, def_obj, rep_id, joint_dict):
#       table = def_obj.get('xls_type')['table']

        # Сначала проверяем, есть ли такой стык в таблице
#       sql = "SELECT * FROM joints WHERE type='%(type)s' AND number='%(number)s' AND sign='%(sign)s' AND kp='%(kp)s' AND diameter='%(diameter)s'" % joint_dict
        sql = "SELECT * FROM joints WHERE type='%(type)s' AND line='%(line)s' AND number='%(number)s' AND sign='%(sign)s' AND kp='%(kp)s'" % joint_dict
        result = self.db.execute(sql)
        row = self.db.cur.fetchall()
        if not row:                         # Если стыка нет в общей таблице, то заносим
            # Общая таблица стыков
            sql = self.joint_query % joint_dict
            try:
                joint_id = self.db.insert(sql)
#               self.f.write("%s\n" % sql)
            except Exception as e:
                self.log(sql)
                return sql
        else:                               # Если есть - извлекаем индекс
            joint_id = row[0][0]

        # Частная таблица стыков (со ссылкой на отчёт)
        joint_dict['_reports_id'] = rep_id
        joint_dict['_joints_id']  = joint_id
        sql = self.entry_query % joint_dict
        try:
            self.db.execute(sql)
#           self.f.write("%s\n" % sql)
        except Exception as e:
            self.log(sql)
            return sql

        # RT-таблица (не реализовано)


    def log(self, str):
        self.f.write("[[[[[[[[[[\n")
        self.f.write("%s\n" % str)
        self.f.write(traceback.format_exc())
        self.f.write("                              ]]]]]]]]]]\n")


    def exec_sql_file(self, sqldir, sqlname):
        filename = os.path.join(self.conf.get('sqlpath'), sqldir, "%s-%s.sql" % (sqlname, self.conf.get('dbtype')))
        if os.path.exists(filename):
            f = file(filename)
            buffer = f.read()
            result = re.split(';', buffer)
            for sql in result:
                try:
                    self.db.execute(sql)
                except Exception as e:
                    logging.exception("Check SQL: {0}".format(sql))
        else:
            logging.warning("No SQL file: {0}".format(filename))
