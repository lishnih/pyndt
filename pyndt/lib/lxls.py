#!/usr/bin/env python
# coding=utf-8
# Stan October 10, 2007

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

"""
Интерфейс XLS_Export для работы c модулем xlrd
"""

import re, time, calendar, logging
import xlrd                     # XLS reader


def str_to_date(date_str):
    try:                    # пытаемся вытащить дату в xls-формате
        date = xlrd.xldate_as_tuple(int(date_str), 0)
        date_str = "%02u.%02u.%02u" % (date[2], date[1], date[0])
    except Exception as e:  # дата, возможно, записана в текстовом формате
        res = re.search(r"(\d{1,2}.\d{1,2}.\d{4})", date_str)
        if res:
            reslist = res.groups()
            date_str = reslist[0]
        else:
            date_str = ""
            val = 0

    if date_str:
        try:
            val = calendar.timegm(time.strptime(date_str, "%d.%m.%Y"))
        except Exception as e:
            val = 0
    return val

# Возращает значение из ячейки [row, col] листа sh
def get_value(sh, row, col):
    try:
        val = sh.cell_value(row, col)
    except Exception as e:
        val = None
    return val


# Сравнивает ячейку [row, col] в листе sh с регулярным выражением match
# Возращает найденное по шаблону или None при ошибке
def contain_value(sh, row, col, seaching_value):
    val = get_value(sh, row, col)
    if val:
        type1 = type(val)
        type2 = type(seaching_value)
        if type1 != type2:
            try:
                val = type2(val)
            except Exception as e:
                print("search: %s (%s)" % (seaching_value, type(seaching_value)))
                print("found:  %s (%s)" % (val, type(val)))
                print("=== contain_value Error! ===")
        if isinstance(seaching_value, basestring):
            result = re.search(seaching_value, val)
            return result
        else:
            if val == seaching_value:
                return True
            else:
                return False
    return None


# Ищет значение search в листе sh
# Возращает [row, col] если нашёл и None в противном случае
# sh - лист; search - значение для поиска
def search_value(sh, seaching_value):
    for i in xrange(sh.ncols):
        for j in xrange(sh.nrows):
            if contain_value(sh, j, i, seaching_value):
                return [j, i]
    return None


# Ищет следующие по горизонтали значения после ячейки с текстом start
# можно задать стоп-ячейку с текстом stop
def search_hor_value(sh, formsearch):
    found = []      # массив найденных значений
    hint = ""
    form_len = len(formsearch)

    if form_len == 2:
        [y0, x0] = formsearch
        str = get_value(sh, y0, x0)
        found.append(str)

    elif form_len == 3:
        [y0, x0, pattern] = formsearch
        str = get_value(sh, y0, x0)
        try:
            res = re.match(pattern, str, re.MULTILINE | re.DOTALL)
        except Exception as e:
            print("Ошибка при выборке данных!")
            print("Проверьте ячейки, из которой извлекаются данные")
            print("Возможно, у Вас неправильно настроен шаблон")
            logging.exception(e)
            print("pattern: %s" % pattern)
            print("str:     %s" % str)
            res = None
        if res:
            reslist = res.groups()
            found.append(reslist[0])
        else:
            hint = str

    elif form_len == 7:
        [start, stop, offset, max, min, empty, ml] = formsearch
        xy = search_value(sh, start)
        if xy != None:                  # если start-ячейка найдена
            y0, x0 = y, x = xy          # координаты
            ind = 0                     # длина массива
            if not offset:              # если надо искать со start-ячейки
                str = get_value(sh, y, x)
                res = re.match(r"([^ ]+) {1,}(.+)", str, re.MULTILINE | re.DOTALL)
                # !!! нужно, чтобы отбрасывалось start
                if res:
                    reslist = res.groups()
                    found.append(reslist[1])
                    ind = 1
#               res = re.split(r" {3,}", str)       # для множественного извлечения
#               if res:
#                   found = res[1:]
#                   ind = len(res) - 1
                else:
                    hint = str
            if not ind:                 # если ещё ничего не найдено
                x += offset
                if x < sh.ncols:
                    for x in range(x, sh.ncols):
                        if ind >= max:
                            break

                        str = get_value(sh, y, x)
                        if str:
                            if stop:
                                res = re.search(stop, unicode(str))     # проба!!!
                                if res:
                                    break
                            found.append(str)
                            ind += 1
                        else:
                            hint = str

                    for i in range(len(found), max):
                        found.append("")

    else:
        print("Неправильный formsearch [%s]" % form_len)

    return [y0, x0, found] if found else hint
