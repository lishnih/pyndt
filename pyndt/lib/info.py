#!/usr/bin/env python
# coding=utf-8

import os

__pkgname__ = os.path.basename(os.path.dirname(os.path.dirname(__file__)))
__description__ = "pyNDT"
__version__ = "0.6"
