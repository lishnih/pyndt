#!/usr/bin/env python
# coding=utf-8
# Stan 2008-07-12

from __future__ import ( division, absolute_import,
                         print_function, unicode_literals )

""" Загрузка фрейма с описаниями событий

Load(framename, options=None)
framename   имя модуля с описанием фрейма
options     для передачи различных параметров
"""

import os, glob, importlib, logging
import wx, wx.xrc               # GUI, XML

import gettext
_ = gettext.gettext


def Load(framename, options=None):
    frame = None
    try:
        mod = __import__(framename)
        frame = mod.Frame(mod, options)
    except Exception as e:
        logging.error(_("Some errors during load Frame: '%s'!"), framename)
        logging.exception(e)
    return frame


def Show(framename, options=None):
    frame = Load(framename, options)
    if frame:
        frame.Show(True)
    return frame


# Этот класс является родительским, для класса, создаваемом в модуле в папке framepath
class Frame(wx.Frame):
    def __init__(self, mod, options=None):
#       wx.Frame.__init__(self, None)
        framename = mod.__name__
        framepath = os.path.dirname(mod.__file__)
        logging.debug(_("Loading frame '%s' with parameters: '%r'"), framename, options)
        logging.debug(_("Dir: '%s'"), framepath)

        # Загружаем объект ID_FRAME из xrc-файла
        resfile = os.path.join(framepath, "frame.xrc")
        res = wx.xrc.XmlResource(resfile)
        w = res.LoadFrame(None, 'ID_FRAME')
        self.PostCreate(w)

        # Распределяем события
        eventsdir = os.path.join(framepath, "events")
        if not os.path.isdir(eventsdir):
            logging.warning(_("Events dir not found: '%s'!"), eventsdir)
            return

        try:
            events = importlib.import_module('.events', framename)
            self.BindAllEvents(events)
        except Exception as e:
            logging.error(_("Some errors during load Events: '%s'!"), eventsdir)
            logging.exception(e)


    def GetEvtModule(self, eventname):
        if eventname in wx.__dict__:
            return getattr(wx, eventname)
        elif eventname in wx.html.__dict__:
            return getattr(wx.html, eventname)
        else:
            return None


    def BindEventFuncs(self, events, itemname = ""):
        eventsname = events.__name__
        eventspath = os.path.dirname(events.__file__)

        for filename in glob.glob(os.path.join(eventspath, "EVT_*.py")):
            basename = os.path.basename(filename)
            root, ext = os.path.splitext(basename)

            module = importlib.import_module('.' + root, eventsname)
            if module:
                logging.debug(_("Event loaded: '%s'!"), module.__file__)
                evtmod = self.GetEvtModule(root)
                if evtmod:
                    if itemname:
                        citem = wx.xrc.XRCID(itemname)
                        self.Bind(evtmod, module.onEvent, id=citem)
                    else:
                        self.Bind(evtmod, module.onEvent)
                else:
                    logging.error(_("Unknown type of event: '%s'!"), eventname)
                module.frame = self
            else:
                logging.error(_("Event not loaded: '%s'!"), filename)


    def BindAllEvents(self, events):
        eventsname = events.__name__
        eventspath = os.path.dirname(events.__file__)

        # В первую очередь распределяем общие события
        self.BindEventFuncs(events)

        # Затем распределяем частные события
        ldir = os.listdir(eventspath)
        for itemname in ldir:
            dirname = os.path.join(eventspath, itemname)
            if os.path.isdir(dirname):
                if os.path.isfile(os.path.join(dirname, "__init__.py")):
                    module = importlib.import_module('.' + itemname, eventsname)
                    self.BindEventFuncs(module, itemname)
                else:
                    logging.info(_("Dir is passed: '%s'!"), dirname)
