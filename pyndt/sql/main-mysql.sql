-- Stan 2007-10-18
-- NDT Reports & Joints Database


CREATE TABLE IF NOT EXISTS `reports` (
  `id`              mediumint(8)        unsigned NOT NULL auto_increment,       -- id
  `enabled`         mediumint(8)        unsigned NOT NULL default '1',          -- Отчёт активен
-- Наименование отчёта
  `name`            varchar(64)         NOT NULL default '',                    -- имя
  `pre`             varchar(12)         NOT NULL default '',                    -- префикс (газ/нефть)
  `seq`             mediumint(8)        unsigned NOT NULL default '0',          -- номер заключения
  `rsign`           varchar(12)         NOT NULL default '',                    -- на случай дробей
-- Параметры
  `method`          varchar(4)          NOT NULL default '',                    -- Тип отчёта
  `q_level`         varchar(12)         NOT NULL default '',                    -- Уровень качества
  `tech_map`        varchar(256)        NOT NULL default '',                    -- Техкарта
  `kp`              mediumint(8)        unsigned NOT NULL default '0',          -- километр
-- `diameter`       mediumint(8)        unsigned NOT NULL default '0',          -- диаметр трубы
  `date`            int(11)             unsigned NOT NULL default '0',          -- дата
  `date_str`        varchar(24)         NOT NULL default '',                    -- дата (текст)
-- `dirname`        varchar(255)        NOT NULL default '',                    -- имя головной директории
  `filename`        varchar(255)        NOT NULL default '',                    -- имя файла
  `joints`          mediumint(8)        unsigned NOT NULL default '0',          -- кол-во стыков
  PRIMARY KEY       (`id`)
-- UNIQUE KEY `report`  (`pre`,`seq`,`rsign`)
-- KEY `rtr_date`       (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='v20130820pgu235' AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `joints` (
  `id`              mediumint(8)        unsigned NOT NULL auto_increment,       -- id
  `kind`            mediumint(8)        NOT NULL default '0',                   -- магистральные и т.д.
-- Наименование стыка
  `name`            varchar(64)         NOT NULL default '',                    -- имя
  `type`            varchar(12)         NOT NULL default '',                    -- префикс 1
  `line`            varchar(20)         NOT NULL default '',                    -- префикс 2
  `number`          mediumint(8)        unsigned NOT NULL default '0',          -- номер стыка
  `sign`            varchar(12)         NOT NULL default '',                    -- буквы после номера
-- Параметры
  `kp`              mediumint(8)        unsigned NOT NULL default '0',          -- километр
  `diameter1`       varchar(24)         NOT NULL default '',                    -- диаметр трубы 1
  `diameter2`       float               unsigned NOT NULL default '0',          -- диаметр трубы 2
  `thickness1`      float               NOT NULL default '0',                   -- толщина стенки 1
  `thickness2`      float               NOT NULL default '0',                   -- толщина стенки 2
  PRIMARY KEY       (`id`)
-- UNIQUE KEY `wid`     (`type`,`line`,`number`,`sign`,`kp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='v20130904pgu235' AUTO_INCREMENT=1;


-- Запись стыка в заключении
CREATE TABLE IF NOT EXISTS `entries` (
  `_reports_id`     mediumint(8)        unsigned NOT NULL default '0',          -- id в reports
  `_joints_id`      mediumint(8)        unsigned NOT NULL default '0',          -- id в joints
  `decision`        varchar(20)         NOT NULL default '',                    -- Заключение
  `extras`          text,                                                       -- доп. поля
  KEY `reports_id`  (`_reports_id`),
  KEY `joints_id`   (`_joints_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='v20130905pgu235';
-- _reports_id - ссылка на номер отчёта из reports
-- _joints_id  - ссылка на номер стыка из joints
