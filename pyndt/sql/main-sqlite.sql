-- Stan 2007-10-18
-- NDT Reports & Joints Database


CREATE TABLE if not exists reports (
  id                INTEGER PRIMARY KEY,    -- id
  enabled           INTEGER default 1,      -- Отчёт активен
-- Наименование отчёта
  name              VARCHAR default '' COLLATE NOCASE,     -- имя
  pre               VARCHAR default '' COLLATE NOCASE,     -- префикс (газ/нефть)
  seq               INTEGER default 0,      -- номер заключения
  rsign             VARCHAR default '' COLLATE NOCASE,     -- на случай дробей
-- Параметры
  method            VARCHAR default '' COLLATE NOCASE,     -- Тип отчёта
  q_level           VARCHAR default '' COLLATE NOCASE,     -- Уровень качества
  tech_map          VARCHAR default '' COLLATE NOCASE,     -- Техкарта
  kp                INTEGER default 0,      -- километр
-- diameter         INTEGER default 0,      -- диаметр трубы
  date              INTEGER default 0,      -- дата
  date_str          VARCHAR default '' COLLATE NOCASE,     -- дата (текст)
-- dirname          VARCHAR default '' COLLATE NOCASE,     -- имя головной директории
  filename          VARCHAR default '',     -- имя файла
  joints            INTEGER default 0       -- кол-во стыков
);


CREATE TABLE if not exists joints (
  id                INTEGER PRIMARY KEY,    -- id
  kind              INTEGER default 0,      -- магистральные и т.д.
-- Наименование стыка
  name              VARCHAR default '' COLLATE NOCASE,     -- имя
  type              VARCHAR default '' COLLATE NOCASE,     -- префикс 1
  line              VARCHAR default '' COLLATE NOCASE,     -- префикс 2
  number            INTEGER default 0,      -- номер стыка
  sign              VARCHAR default '' COLLATE NOCASE,     -- буквы после номера
-- Параметры
  kp                INTEGER default 0,      -- километр
  diameter1         INTEGER default 0,      -- диаметр трубы 1
  diameter2         INTEGER default 0,      -- диаметр трубы 2
  thickness1        FLOAT default 0,        -- толщина стенки 1
  thickness2        FLOAT default 0         -- толщина стенки 2
);


-- Запись стыка в заключении
CREATE TABLE if not exists entries (
  _reports_id       INTEGER default 0,      -- id в reports
  _joints_id        INTEGER default 0,      -- id в joints
  decision          VARCHAR default '' COLLATE NOCASE,     -- Заключение
  extras            TEXT                    -- доп. поля
);
-- _reports_id - ссылка на номер отчёта из reports
-- _joints_id  - ссылка на номер стыка из joints
